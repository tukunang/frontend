{{/*
Expand the name of the chart.
*/}}
{{- define "partnership-dashboard-frontend.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "partnership-dashboard-frontend.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "partnership-dashboard-frontend.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "partnership-dashboard-frontend.labels" -}}
helm.sh/chart: {{ include "partnership-dashboard-frontend.chart" . }}
{{ include "partnership-dashboard-frontend.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "partnership-dashboard-frontend.selectorLabels" -}}
app.kubernetes.io/name: {{ include "partnership-dashboard-frontend.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "partnership-dashboard-frontend.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "partnership-dashboard-frontend.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}


{{- define "partnership-dashboard-frontend.env" -}}
- name: REACT_APP_BACKEND_API_URL
  value: {{ .Values.api.url }}
- name: REACT_GOOGLE_ANALYTIC
  value: "{{ .Values.thirdParty.google.analytic.key }}"

{{- end }}