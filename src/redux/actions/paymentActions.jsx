// @flow

import { PaymentConstants } from "../constanta/PaymentContant";
import authFetch from "../helpers/authFetch";
import { authHeader } from "../helpers/authHeader";
import { CekHttpNetworkResponse } from "../helpers/cekResponse";
import { API_URL, handleResponse } from "../helpers/handleResponse";
import { ErrorResponse } from "../helpers/response";
import { payment } from "../utils/paymentUtils";

function createPaymentMethod(data: any, type) {
  return (dispatch) => {
    dispatch(request(data));
    payment
      .createPayment(data, type)
      .then((payment) => {
        dispatch(success(payment));
      })
      .catch((error: ErrorResponse) => {
        dispatch(failure(error));
      });
  };
  function request(payment) {
    return { type: PaymentConstants.PAYMENT_REQUEST, payment };
  }
  function success(payment) {
    return { type: PaymentConstants.PAYMENT_SUCCESS, payment };
  }
  function failure(error) {
    return { type: PaymentConstants.PAYMENT_FAILURE, error };
  }
}

function listPaymentMethod() {
  const requestOptions = {
    method: 'GET',
    headers: authHeader(),
  };
  return (dispatch) => {
    dispatch(request());
    return authFetch(`account/payout`, requestOptions)
    .then((listPayment) => {
      dispatch(success(listPayment));
    });
  };
  function request() {
    return { type: PaymentConstants.PAYMENTS_REQUEST };
  }
  function success(listPayment) {
    return { type: PaymentConstants.PAYMENTS_SUCCESS, listPayment };
  }
  function failure(error) {
    return { type: PaymentConstants.PAYMENTS_FAILURE, error };
  }
}

function updatePaymentMethod(data: any, type: string, id: string) {
  const requestOptions = {
    method: "PUT",
    headers: { ...authHeader(), "Content-Type": "application/json" },
    body: JSON.stringify({ data, type }),
  };
  return (dispatch) => {
    dispatch(request(data, id));
    return fetch(`${API_URL}/account/payout/update/${id}`, requestOptions).then(CekHttpNetworkResponse).then(handleResponse)
      .then((payment) => {
        dispatch(success(payment));
      })
      .catch((error: ErrorResponse) => {
        dispatch(failure(error));
      });
  };
  function request(payment) {
    return { type: PaymentConstants.PAYMENTS_UPDATE_REQUEST, payment };
  }
  function success(payment) {
    return { type: PaymentConstants.PAYMENTS_UPDATE_SUCCESS, payment };
  }
  function failure(error) {
    return { type: PaymentConstants.PAYMENT_FAILURE, error };
  }
}

function deletePaymentMethod(id: string) {
  const requestOptions = {
    method: "DELETE",
    headers: authHeader(),
  };
  return (dispatch) => {
    dispatch(request(id));
    return fetch(`${API_URL}/account/payout/delete/${id}`, requestOptions)
    .then(CekHttpNetworkResponse).then(handleResponse)
    .then(
      (payment) => {
        dispatch(success(id));
      },
      (error) => dispatch(failure(id, error)),
    );
  };
  function request(id) {
    return { type: PaymentConstants.DELETE_REQUEST, id };
  }
  function success(id) {
    return { type: PaymentConstants.DELETE_SUCCESS, id };
  }
  function failure(id, error) {
    return { type: PaymentConstants.DELETE_FAILURE, id, error };
  }
}

export const paymentActions = {
  createPaymentMethod,
  listPaymentMethod,
  updatePaymentMethod,
  deletePaymentMethod,
};
