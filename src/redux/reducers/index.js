import themeReducer from "./themeReducer";
import sidebarReducer from "./sidebarReducer";
import rtlReducer from "./rtlReducer";
import authentication from "./AuthReducer";
import registration from "./RegistrationReducer";
import partnerAffiliate from "./AffiliateReducer";
import userProfile from "./UserReducer";
import partnerReseller from "./ResellerReducer";
import paymentMethod from "./PaymentReducer";
import forgetPassword from './ForgetPassword';

export {
  themeReducer,
  sidebarReducer,
  rtlReducer,
  authentication,
  forgetPassword,
  registration,
  partnerAffiliate,
  userProfile,
  partnerReseller,
  paymentMethod,
};
