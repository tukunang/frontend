// @flow

type FetchRespon = {
    ok: boolean,
    json: () => Promise<{}>,
    text: () => Promise<string>,
    status: number,
    headers: {
        get: (key: string) => string
    },
};

class ErrorResponse {
    constructor(response: FetchRespon, json= null) {
        const defaultJson = { nonFieldErrors: ['Unknown error occurred'] };
        this.response = response;
        this.json = json || defaultJson;
    }

    json: any;

    response: FetchRespon;
}

export { ErrorResponse };
export type { FetchRespon };