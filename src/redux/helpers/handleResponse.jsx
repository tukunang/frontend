// @flow

import type { FetchRespon } from "./response";

export const handleResponse = (response: FetchRespon) => {
  const contentType = response.headers.get('content-type');
  if (contentType && contentType.indexOf('application/json') !== -1) {
    return response.json();
  }
  return response.text();
};
let BACKEND_API_URL;

if (window.location.hostname === "dev-partners.blankontech.com") {
  BACKEND_API_URL = "https://dev-api-partners.blankontech.com";
} else if (window.location.hostname === "staging-partners.blankontech.com") {
  BACKEND_API_URL = "https://staging-api-partners.blankontech.com";
} else if (window.location.hostname === "partners.blankontech.com") {
  BACKEND_API_URL = "https://api-partners.blankontech.com";
} else {
  // BACKEND_API_URL = "http://192.168.10.4:8002";
  // BACKEND_API_URL = "http://localhost:8002";
  BACKEND_API_URL = "https://staging-api-partners.blankontech.com";
}

export const API_URL = BACKEND_API_URL;
