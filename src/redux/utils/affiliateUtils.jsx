// @flow

import authFetch from "../helpers/authFetch";
import { authHeader } from "../helpers/authHeader";

async function AffiliateProject(): Promise<void> {
    const requestOptions = {
        method: 'GET',
        headers: authHeader(),
    };

    return authFetch(`partner/projects`, requestOptions);
}

export const Affiliate = {
    AffiliateProject,
};