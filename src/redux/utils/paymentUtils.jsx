// @flow

import authFetch from "../helpers/authFetch";
import { authHeader } from "../helpers/authHeader";
import { CekHttpNetworkResponse } from "../helpers/cekResponse";
import { API_URL, handleResponse } from "../helpers/handleResponse";

async function createPayment(data: any, type): Promise<void> {
  const requestOptions = {
    method: "POST",
    headers: { ...authHeader(), "Content-Type": "application/json" },
    body: JSON.stringify({ type, data }),
  };

  const createPayments = await fetch(
    `${API_URL}/account/payout/create`,
    requestOptions,
  ).then(CekHttpNetworkResponse).then(handleResponse);
  return createPayments;
}

async function updatePayment(data: any, id: string): Promise<void> {
  const requestOptions = {
    method: "PUT",
    headers: { ...authHeader(), "Content-Type": "application/json" },
    body: JSON.stringify({ data }),
  };

  const createPayments = await fetch(
    `${API_URL}/account/payout/update/${id}`,
    requestOptions,
  ).then(CekHttpNetworkResponse).then(handleResponse);
  return createPayments;
}

export const payment = {
  createPayment,
  updatePayment,
};
