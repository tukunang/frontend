// @flow

import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHistory } from "@fortawesome/free-solid-svg-icons";
import { Panel } from "../../../shared/components";
import TotalStatusChart from "../../../shared/components/TotalStatusChart";

// eslint-disable-next-line react/prefer-stateless-function
class TotalStatusClientReseller extends React.Component {
  render() {
    const { data } = this.props;
    return (
      <>
        <Panel xs={12} md={12} lg={12} xl={4}>
          {data.length === 0 ? (
            <div>
            <h3 style={{ justifyContent: "center", display: "flex" }}>
              <FontAwesomeIcon icon={faHistory} />
            </h3>
            <span
              style={{
                justifyContent: "center",
                display: "flex",
                marginTop: "12px",
              }}
            >
              You don&apos;t have any client
            </span>
            </div>
          ):(
            <TotalStatusChart data01={data} />
          )}
        </Panel>
      </>
    );
  }
}

TotalStatusClientReseller.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    cost: PropTypes.string,
    status: PropTypes.number,
  })),
};

TotalStatusClientReseller.defaultProps = {
  data: [],
};

export default TotalStatusClientReseller;
