// @flow

import React from "react";
import PropTypes from "prop-types";
// import CashIcon from "mdi-react/CashIcon";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBalanceScale, faChartLine, faDollarSign, faMoneyBill, faPercent, faRocket } from "@fortawesome/free-solid-svg-icons";
import { PanelCommision, PanelCompare, PanelPayout } from "../../../shared/components/Panel";

type State = {
  open: boolean,
  paypal: boolean,
  status: string,
};

// eslint-disable-next-line react/prefer-stateless-function
class AffiliateAmount extends React.Component<null, State> {
  state = {
    open: false,
    paypal: false,
    status: 0,
  };

  handleModal = () => {
    this.setState((prevState) => ({
      open: !prevState.open,
    }));
  };

  handlePayPal = () => {
    this.setState((prevState) => ({
      paypal: !prevState.paypal,
    }));
  };

  radioHandler = () => {
    this.setState((prevState) => ({
      status: prevState.status,
    }));
  };

  render() {
    const {
      totalAmount, lastAmount, compareAmount, amount,
    } = this.props;
    const StringTotal = totalAmount
      .map((list) => list.commision)
      .reduce((total, commision) => total + parseInt(commision, 10), 0);
    const today = new Date();
    const lastMonth = today.getMonth()-1;
    const Month = today.getMonth();
    const total = lastAmount.filter(i => {
      const date = new Date(i.created_at);
      return date.getMonth() === lastMonth;
    }).reduce((prev, amount) => prev + parseInt(amount.amount, 10), 0);
    const totalCompare = compareAmount.filter(i => {
      const date = new Date(i.created_at);
      return date.getMonth() === Month;
    }).reduce((prev, amount) => prev + parseInt(amount.amount, 10), 0);
    let compare = 0;
    let compareMinus = 0;
    if (Month !== lastMonth) {
      compareMinus = totalCompare - total;
      const comparePlus = totalCompare + total;
      compare = Math.abs((compareMinus / comparePlus)) * 100;
    }
    return (
      <>
        <PanelCommision md={12} xl={3} lg={6} xs={12} title="total Commission" header>
          <div dir="ltr">
            <div className="dashboard__weight-stats">
              <div className="card-affiliate-Money">
                <FontAwesomeIcon className="card-money" icon={faMoneyBill} />
              </div>
                <FontAwesomeIcon className="card-dollar" icon={faChartLine} />
            </div>
            <div className="card-panel-amount">
              <FontAwesomeIcon style={{ marginRight: "12px" }} icon={faDollarSign} />
              <h3 className="dashboard__weight-stat-title card-panel-amount-text">{StringTotal}</h3>
            </div>
          </div>
        </PanelCommision>
        <PanelPayout
          md={12}
          xl={3}
          lg={6}
          xs={12}
          title="Last Payout By Month"
          header
        >
          <div dir="ltr">
            <div className="dashboard__weight-stats">
              <div className="card-affiliate-payout">
                <FontAwesomeIcon className="card-rocket" icon={faRocket} />
              </div>
                <FontAwesomeIcon className="card-dollar" icon={faChartLine} />
            </div>
            <div className="card-panel-amount">
              <FontAwesomeIcon style={{ marginRight: "12px" }} icon={faDollarSign} />
              <h3 className="dashboard__weight-stat-title card-panel-amount-text"> {amount === null ? 0 : total}</h3>
            </div>
          </div>
        </PanelPayout>
        <PanelCompare md={12} xl={3} lg={6} xs={12} title="Compare Commission" header>
          <div dir="ltr">
            <div className="dashboard__weight-stats">
              <div className="card-affiliate-compare">
                <FontAwesomeIcon className="card-compare" icon={faBalanceScale} />
              </div>
                <FontAwesomeIcon className="card-dollar" icon={faChartLine} />
            </div>
            <div className="card-panel-amount">
              <FontAwesomeIcon style={{ marginRight: "12px" }} icon={faDollarSign} />&nbsp;
              <h3 className="dashboard__weight-stat-title card-panel-amount-text">{compareMinus}</h3>
              {/* &nbsp;({isNaN(compare) ? 0 : compare}&nbsp;<FontAwesomeIcon icon={faPercent} />) */}
            </div>
          </div>
        </PanelCompare>
      </>
    );
  }
}

AffiliateAmount.propTypes = {
  totalAmount: PropTypes.node,
  lastAmount: PropTypes.node,
  compareAmount: PropTypes.number,
  amount: PropTypes.node,
};

AffiliateAmount.defaultProps = {
  totalAmount: [],
  amount: [],
  lastAmount: [],
  compareAmount: [],
};

const AffiliateProjectRedux = AffiliateAmount;
export { AffiliateProjectRedux as AffiliateAmount };
