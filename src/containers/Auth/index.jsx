import LoginPage from './views/LoginPage';
import RegisterPage from './views/RegisterPage';
import ResetPasswordPage from './views/ResetPasswordPage';

export {
  LoginPage,
  ResetPasswordPage,
  RegisterPage,
};