// @flow

import React from "react";
import { Switch, withRouter } from "react-router-dom";
import Layout from "../Layout/index";
import { AffiliateView } from "../Affiliate/views/AffiliateView";
import { ResellerDashboard } from "../Resellers/views/ResellerDashboard";
import MenuBarProfiles from "../Profiles/views/MenuBarProfile";
import EditProfiles from "../Profiles/views/EditProfileView";
import PrivateRoute from "./PrivateRouter";
import ProfileViews from "../Profiles/views/ProfileView";
import Payments from "../Profiles/Billing/Payment";
import withPageView from "../../shared/components/GoggleAnalytic";

type RouteDef = {
  path: string,
  component: React.ComponentType,
  exact: boolean,
};

const RoutePages: RouteDef[] = [
  {
    path: '/affiliate',
    component: AffiliateView,
    exact: true,
  },
  {
    path: '/overview',
    component: MenuBarProfiles,
    exact: true,
  },
  {
    path: '/reseller',
    component: ResellerDashboard,
    exact: true,
  },
  {
    path: '/edit-profile',
    component: EditProfiles,
    exact: true,
  },
  {
    path: '/profile',
    component: ProfileViews,
    exact: true,
  },
  {
    path: '/payment',
    component: Payments,
    exact: true,
  },
];

function Pages() {
    return (
      <div>
        <Switch>
        <div>
          <Layout />
          <div className="container__wrap">
            {RoutePages.map((u) => (
              <PrivateRoute
                key={u.path}
                path={u.path}
                exact={u.exact}
                component={withPageView((props) => <u.component {...props} />)}
              />
            ))}
          </div>
          {/* <Route path="*" component={NotFound} /> */}
        </div>
        </Switch>
      </div>
    );
}

export default withRouter(Pages);
