import React from "react";
import {
  Route, Switch, withRouter, Redirect,
} from "react-router-dom";
import MainWrapper from "./MainWrapper";
import { LoginPage, RegisterPage, ResetPasswordPage } from "../Auth";
import ForgetPasswordPage from "../Auth/views/ForgetPasswordPage";
import Pages from "./Pages";
import { Tos } from "../Layout/TOS";

// eslint-disable-next-line react/prefer-stateless-function
class Routers extends React.Component {
  render() {
    return (
      <MainWrapper>
        <Switch>
          <Route
            path="/login"
            exact
            render={(props) => <LoginPage {...props} />}
          />
          <Route
            path="/register"
            exact
            render={(props) => <RegisterPage {...props} />}
          />
          <Route
            path="/terms-condition"
            exact
            render={(props) => <Tos {...props} />}
          />
          <Route exact path="/forget-password" component={ForgetPasswordPage} />
          <Route exact path="/password-reset" component={ResetPasswordPage} />
          <Redirect exact from="/" to="/affiliate" />
          <Pages />
        </Switch>
      </MainWrapper>
    );
  }
}

const RoutersComponent = withRouter(Routers);

export { RoutersComponent as Routers };
