import React from "react";
import { Container, Row } from "reactstrap";
import { Link } from "react-router-dom";

const notFound = `${process.env.PUBLIC_URL}/img/notFound.svg`;

// eslint-disable-next-line react/prefer-stateless-function
class NotFound extends React.Component {
  render() {
    return (
      <div className="not-found">
        <main>
          <Container style={{ marginTop: "70px", marginBottom: "70px" }}>
            <Row>
              <div className="col-md-6 align-self-center">
                <img src={notFound} alt="" />
              </div>
              <div className="col-md-6 align-self-center">
                <h1>404</h1>
                <h2>UH OH! You&apos;re lost.</h2>
                <p>
                  The page you are looking for does not exist. How you got here
                  is a mystery. But you can click the button below to go back to
                  the homepage.
                </p>
                <Link to="/login">
                <button type="submit" className="btn-green green">Back</button>
                </Link>
              </div>
            </Row>
          </Container>
        </main>
      </div>
    );
  }
}

export default NotFound;
