// @flow

import React from "react";
import ArrowUpIcon from "mdi-react/ArrowUpIcon";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import AccountCircleIcon from "mdi-react/AccountCircleIcon";
import ChartTimelineVariantIcon from "mdi-react/ChartTimelineVariantIcon";
// import Skeleton from '@mui/material/Skeleton';
import { Panel } from "../../../shared/components";
import ProfileMenu from "./Menu/MenuLinks/ProfileMenu";
// import CardTos from "../views/CardTos";

// eslint-disable-next-line react/prefer-stateless-function
class ProfileDetail extends React.Component {
  render() {
    const {
      items: { results },
      items, itemsResseler, last_name, first_name, children,
    } = this.props;
    const StringTotal = results && results.map(list => list.commision).reduce(
      (total, commision) => total + parseInt(commision, 10),
      0,
    );
    const totalReseller = itemsResseler.results && itemsResseler.results.map(list => list.cost).reduce(
      (total, cost) => total + parseInt(cost, 10),
      0,
    );
    const total = StringTotal + totalReseller;
    const project = items.count + itemsResseler.count;
    return (
      <>
      <Panel xs={12} md={12} lg={12}>
      {/* <Panel xl={9} lg={8} md={12} xs={12}> */}
        {/* {this.props.isLoading ? (
           <Skeleton animation="wave" variant="circular" width={100} height={100} />
        ):( */}
          <>
          <div className="profile__information">
          <div className="profile__avatar">
            <AccountCircleIcon size="140px" />
          </div>
          <div className="profile__data">
            <div className="profile__name">
              {first_name}&nbsp; {last_name}
            </div>
            <div className="profile__flex__status">
              <span className="profile__status">Reseller</span>
              <span className="profile__status">Affiliate</span>
            </div>
            <div className="profile__project">
              <div className="profile__border__projects">
                <div className="profile__flex_border_icon">
                  <span className="profile__border__icon__up">
                    <ArrowUpIcon />
                  </span>
                  <span className="profile__text_border">${total}</span>
                </div>
                <h4 className="profile__text__border_down">Amount</h4>
              </div>
              <div className="profile__border__projects">
                <div className="profile__flex_border_icon">
                  <span className="profile__border__icon__up">
                    <ChartTimelineVariantIcon />
                  </span>
                  <span className="profile__text_border">{project}</span>
                </div>
                <h4 className="profile__text__border_down">Project</h4>
              </div>
            </div>
          </div>
          </div>
          <div>
          <ProfileMenu />
          </div>
          </>
        {/* )} */}
      </Panel>
      {/* <CardTos /> */}
      {children}
      </>
    );
  }
}

ProfileDetail.propTypes = {
  children: PropTypes.node.isRequired,
  last_name: PropTypes.string.isRequired,
  first_name: PropTypes.string.isRequired,
};

function mapStateToProps(state) {
  const { partnerAffiliate, partnerReseller, userProfile } = state;
  const { items } = partnerAffiliate;
  const { itemsResseler } = partnerReseller;
  const { isLoading } = userProfile;
  return { items, itemsResseler, isLoading };
}

const profileComponent = connect(
  mapStateToProps,
)(ProfileDetail);
export { profileComponent as ProfileDetail };
