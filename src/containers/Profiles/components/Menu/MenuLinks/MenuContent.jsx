import React from "react";
import ProfileLink from "./ProfileLinks";

function MenuContent() {
  return (
    <div className="sidebar__content">
      <span className="profile__block">
        <ProfileLink
          title="Overview"
          route="/overview"
        />
        <ProfileLink
          title="Settings"
          route="/edit-profile"
        />
        <ProfileLink
          title="Payment"
          route="/payment"
        />
      </span>
    </div>
  );
}

export default MenuContent;
