// @flow

import React from "react";
import CircularProgress from "@mui/material/CircularProgress";
import { connect } from "react-redux";
import { paymentActions } from "../../../redux/actions/paymentActions";
import ModalDetailPayment from "./ModalDetailPayment";
import ModalFormPayment from "./ModalMenagementPayment";
import ModalFormBank from "./ModalFormBank";
import ModalFormPayPal from "./ModalFormPaypal";
import { IconModal } from "../../../shared/components/IconModal";
import { ModalDelete } from "../../../shared/components/ModalDelete";
import CardPayPal from "./CardPayPal";
import CardBank from "./CardBank";
import { maxLengthCheck, Slice } from "../../../shared/helpers";

type PayPal = {
  email: string,
};

type Bank = {
  bank_name: string,
  routing_number: string,
  account_number: string,
  bic: string,
  iban: string,
};

type State = {
  showModal: string,
  toggle: string,
  updatePayment: string,
  popover: boolean,
  bankPayments: Bank,
  paypal: PayPal,
  submitted: boolean,
  ErrorMessage: any,
  submitted2: boolean,
};

// eslint-disable-next-line react/prefer-stateless-function
class PaymentList extends React.Component<null, State> {
  state = {
    paypal: { email: "" },
    bankPayments: {
      bank_name: "",
      routing_number: "",
      account_number: "",
      name: "",
      bic: "",
      iban: "",
    },
    currency: [
      {
        name: "USD",
        value: "USD",
      },
      {
        name: "EURO",
        value: "EURO",
      },
    ],
    selectedValue: "USD",
    showModal: 0,
    usd: "usd",
    euro: "euro",
    toggle: 0,
    type: 1,
    type2: 2,
    updatePayment: 0,
    submitted2: false,
    ErrorMessage: {},
    popover: false,
    submitted: false,
  };

  componentDidMount() {
    this.props.getPayment();
    // this.handleModalUpdate();
  }

  selectedValueHandler = (e) => {
    this.setState({ selectedValue: e.target.value });
  };

  handleChangePayPal = (event) => {
    const { name, value } = event.target;
    this.setState((prevState) => ({
      paypal: {
        ...prevState.paypal,
        [name]: value,
      },
    }));
  };

  handleChangeBank = (event) => {
    const { name, value } = event.target;
    this.setState((prevState) => ({
      bankPayments: {
        ...prevState.bankPayments,
        [name]: value,
      },
    }));
  };

  getModal(id) {
    return this.setState({ showModal: id });
  }

  hideModal(id) {
    return this.setState({ showModal: 0 });
  }

  handleModal(id) {
    return this.setState({ toggle: id });
  }

  handleModalUpdate(id, data) {
    if (data.usd) {
      this.setState({
        updatePayment: id,
        bankPayments: {
            bank_name: data.bank_name,
            routing_number: data.routing_number,
            account_number: data.account_number,
            bic: "",
            name: data.name,
            iban: "",
        },
        paypal: {
          email: data.email,
        },
        selectedValue: "USD",
        currency: [
          {
            name: "USD",
            value: "USD",
          },
          {
            name: "EURO",
            value: "EURO",
          },
        ],
      });
    } else {
      this.setState({
        updatePayment: id,
        bankPayments: {
            bank_name: data.bank_name,
            routing_number: "",
            account_number: "",
            bic: data.bic,
            name: data.name,
            iban: data.iban,
        },
        selectedValue: "EURO",
        currency: [
          {
            name: "EURO",
            value: "EURO",
          },
          {
            name: "USD",
            value: "USD",
          },
        ],
        paypal: {
          email: data.email,
        },
      });
    }
  }

  handleCloseUpdate(id) {
    return this.setState({ updatePayment: 0 });
  }

  handleModalHide(id) {
    return this.setState({ toggle: 0 });
  }

  handlePopover = () => {
    this.setState((prevState) => ({
      popover: !prevState.popover,
    }));
  };

  handleSubmitBank = (id) => (event) => {
    event.preventDefault();
    const {
      bankPayments, type, usd, euro, selectedValue,
    } = this.state;
    this.setState({ submitted: true });
    if (
      selectedValue === "USD" && type && bankPayments.routing_number && bankPayments.bank_name && bankPayments.account_number && bankPayments.name
    ) {
      if (
        bankPayments.routing_number === bankPayments.routing_number && bankPayments.bank_name === bankPayments.bank_name
      ) {
        const banksUsd = Slice(bankPayments, ["bank_name", "account_number", "name", "routing_number"]);
        const bankUSD = { ...banksUsd, usd };
        this.props.updatePayment(bankUSD, type, id);
        this.setState({
          open: false,
          updatePayment: 0,
        });
      }
    } else if (
      selectedValue === "EURO" && type && bankPayments.bic && bankPayments.bank_name && bankPayments.iban && bankPayments.name
    ) {
      if (
        bankPayments.bic === bankPayments.bic && bankPayments.iban === bankPayments.iban
      ) {
        const banksEuro = Slice(bankPayments, ["bank_name", "iban", "name", "bic"]);
        const bankEuro = { ...banksEuro, euro };
        this.props.updatePayment(bankEuro, type, id);
        this.setState({
          open: false,
          updatePayment: 0,
        });
      }
    }
  };

  handleChangeSubmit = (id) => (event) => {
    event.preventDefault();
    const { paypal, type2 } = this.state;
    this.setState({ submitted2: true });
    if (!this.isValidEmail()) {
      this.setState({
        ErrorMessage: {
          email: "This is not a valid email",
        },
      });
      return;
    }
    if (paypal.email) {
      this.props.updatePayment(paypal, type2, id);
      this.setState({
        updatePayment: 0,
      });
    }
  };

  handleDeletePayment = (id) => {
    this.props.deletePayment(id);
    this.setState({ toggle: 0 });
  };

  isValidEmail() {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(this.state.paypal.email).toLowerCase());
  }

  render() {
    const {
      showModal, toggle, bankPayments: { name, bank_name, bic, iban, routing_number, account_number }, paypal: { email },
      submitted, currency, submitted2,
    } = this.state;
    const { items, update } = this.props;
    if (update) {
      return (
        <div
          style={{ marginTop: "20px" }}
          className="card-payment__loading-delete"
        >
          <CircularProgress />
        </div>
      );
    }
    return (
      <>
        <div className="row">
          {items && items.length > 0 && items.map((item, index) => (
              <div style={{ paddingRight: "8px" }} key={item.id}>
                <div>
                  {item.type === 1 && item.type !== "" ? (
                    <>
                      <CardBank
                        card_numer={item.data.account_number || item.data.iban}
                        bank_name={item.data.bank_name}
                        name={item.data.name}
                        currency={item.data.usd === "usd" ? <>usd</> : <>euro</>}
                      >
                        <IconModal
                          getModal={() => this.getModal(item.id)}
                          updateModal={() => this.handleModalUpdate(item.id, item.data)}
                          editModal={() => this.handleModal(item.id)}
                          delete_icon={item.id}
                          edit_icon={item.id}
                          detail_icon={item.id}
                        />
                      </CardBank>
                    </>
                  ) : (
                    ""
                  )}
                  {item.type === 2 && item.type !== "" ? (
                    <>
                      <CardPayPal email={item.data.email}>
                        <IconModal
                          getModal={() => this.getModal(item.id)}
                          updateModal={() => this.handleModalUpdate(item.id, item.data)}
                          editModal={() => this.handleModal(item.id)}
                          delete_icon={item.id}
                          edit_icon={item.id}
                          detail_icon={item.id}
                        />
                      </CardPayPal>
                    </>
                  ) : (
                    ""
                  )}
                  {item.deleting ? (
                    <div
                      style={{ marginTop: "20px" }}
                      className="card-payment__loading-delete"
                    >
                      <CircularProgress />
                    </div>
                  ) : (
                    <>
                      <ModalDelete
                        toggle={toggle === item.id}
                        handleModalHide={() => this.handleModalHide(item.id)}
                        handleDeletePayment={() => this.handleDeletePayment(item.id)}
                      />
                    </>
                  )}
                    <ModalFormPayment
                      toggle={this.state.updatePayment === item.id}
                      handleModal={() => this.handleCloseUpdate(item.id)}
                      onSubmit={
                        item.type === 1 ? this.handleSubmitBank(item.id) : this.handleChangeSubmit(item.id)
                      }
                      type={item.type}
                      updateYourData
                      addPayment={false}
                    >
                      {item.type === 1 && item.type !== "" ? (
                        <>
                        <div className="form__form-group">
                          <span className="form__form-group-label">Currency</span>
                          <div className="form__form-group-select">
                            <div className="form__form-group-input-wrap">
                              <select
                                id="searchSelect"
                                name="searchSelect"
                                onChange={this.selectedValueHandler}
                                className="form__select"
                              >
                                {currency.map((item) => (
                                  <option key={item.value}>{item.name}</option>
                                ))}
                              </select>
                            </div>
                          </div>
                        </div>
                        <ModalFormBank
                          submitted={submitted}
                          selectedValue={this.state.selectedValue}
                          maxLength={maxLengthCheck}
                          onChange={(bank) => this.setState(bank)}
                          errors={this.state.ErrorMessage.bank && (
                            <div
                              id="validationServer03Feedback"
                              className="form__form-group-error"
                            >
                              {this.state.ErrorMessage.bank}
                            </div>
                          )}
                          errorsEuro={
                            this.state.ErrorMessage.bankEuro && (
                              <div
                                id="validationServer03Feedback"
                                className="form__form-group-error"
                              >
                                {this.state.ErrorMessage.bankEuro}
                              </div>
                            )
                          }
                          bankPayments={{ bank_name, name, iban, bic, routing_number, account_number }}
                        />
                        <button
                          className="btn btn-account account__btn account__btn--small"
                          style={{
                            marginTop: "10px",
                            width: "100%",
                            color: "#fff",
                            position: "relative",
                          }}
                          type="submit"
                        >
                          Save
                        </button>
                        </>
                      ) : (
                        <>
                          <ModalFormPayPal
                            submitted={submitted2}
                            paypal={{ email }}
                            onChange={(paypal) => this.setState(paypal)}
                          />
                          {this.state.ErrorMessage.check_email && (
                            <div
                              id="validationServer03Feedback"
                              className="form__form-group-error"
                            >
                              {this.state.ErrorMessage.check_email}
                            </div>
                          )}
                          <button
                            className="btn btn-account account__btn account__btn--small"
                            style={{
                              marginTop: "10px",
                              color: "#fff",
                              position: "relative",
                              width: "100%",
                            }}
                            type="submit"
                          >
                            Save
                          </button>
                        </>
                      )}
                    </ModalFormPayment>
                </div>
                <ModalDetailPayment
                  toggle={showModal === item.id}
                  handleModal={() => this.hideModal(item.id)}
                  type={item.type}
                  usd={item.data.usd}
                  bank_name={item.data.bank_name}
                  routing_number={item.data.routing_number}
                  account_number={item.data.account_number}
                  iban={item.data.iban}
                  email={item.data.email}
                  bic={item.data.bic}
                  name={item.data.name}
                />
              </div>
            ))}
        </div>
      </>
    );
  }
}

function mapState(state) {
  const {
    items, loading, update,
  } = state.paymentMethod;
  return {
    items, loading, update,
  };
}

const actionCreators = {
  deletePayment: paymentActions.deletePaymentMethod,
  getPayment: paymentActions.listPaymentMethod,
  updatePayment: paymentActions.updatePaymentMethod,
};

const Payment = connect(mapState, actionCreators)(PaymentList);

export { Payment as PaymentList };
