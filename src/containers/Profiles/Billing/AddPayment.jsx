// @flow

import React from "react";
import PlusIcon from "mdi-react/PlusIcon";
import { Button } from "reactstrap";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { paymentActions } from "../../../redux/actions/paymentActions";
import ModalFormPayment from "./ModalFormPayment";
import ModalFormBank from "./ModalFormBank";
import ModalFormPayPal from "./ModalFormPaypal";
import { maxLengthCheck, Slice } from "../../../shared/helpers";
import { payment } from "../../../redux/utils/paymentUtils";

type PayPal = {
  email: string,
};

type USD = {
  usd: string,
};

type EURO = {
  euro: string,
};

type Bank = {
  bank_name: string,
  routing_number: string,
  account_number: string,
  bic: string,
  name: string,
  iban: string,
};

type State = {
  currency: any,
  paypal: PayPal,
  open: boolean,
  bankPayments: Bank,
  selectedValue: string,
  type2: string,
  submitted: boolean,
  submitted2: boolean,
  type: string,
  usd: USD,
  euro: EURO,
  ErrorMessage: any,
};

// eslint-disable-next-line react/prefer-stateless-function
class AddPayment extends React.Component<null, State> {
  state = {
    paypal: { email: "" },
    selectedValue: "USD",
    bankPayments: {
      routing_number: "",
      account_number: "",
      bank_name: "",
      bic: "",
      iban: "",
      name: "",
    },
    currency: [
      {
        name: "USD",
        value: "USD",
      },
      {
        name: "EURO",
        value: "EURO",
      },
    ],
    type: 2,
    usd: "usd",
    euro: "euro",
    type2: 1,
    submitted: false,
    submitted2: false,
    ErrorMessage: {},
    open: false,
  };

  selectedValueHandler = (e) => {
    this.setState({ selectedValue: e.target.value, ErrorMessage: {} });
  };

  handleModal = () => {
    this.setState((prevState) => ({
      open: !prevState.open,
    }));
  };

  handleSubmitBank = (event) => {
    event.preventDefault();
    const {
      bankPayments, type2, usd, euro, selectedValue,
    } = this.state;
    this.setState({ submitted: true });
    if (
      selectedValue === "USD" && bankPayments.routing_number && bankPayments.bank_name && bankPayments.account_number && bankPayments.name
    ) {
        const banksUsd = Slice(bankPayments, ["bank_name", "account_number", "name", "routing_number"]);
        const bankUSD = { ...banksUsd, usd };
        this.props.createPayment(bankUSD, type2);
        this.setState({
          open: false,
          submitted: false,
        });
    } else if (
      selectedValue === "EURO" && bankPayments.bic && bankPayments.bank_name && bankPayments.iban && bankPayments.name
    ) {
        const banksEuro = Slice(bankPayments, ["bank_name", "iban", "name", "bic"]);
        const bankEuro = { ...banksEuro, euro };
        this.props.createPayment(bankEuro, type2);
        this.setState({
          open: false,
          submitted: false,
        });
  }
  };

  handleChangeSubmit = (event) => {
    event.preventDefault();
    const { paypal, type } = this.state;
    this.setState({ submitted2: true });
    if (!this.isValidEmail()) {
      this.setState({
        ErrorMessage: {
          email: "This is not a valid email",
        },
      });
      return;
    }
    if (paypal.email) {
      this.props.createPayment(paypal, type);
      this.setState({
        updatePayment: 0,
      });
    }
  };

  isValidEmail() {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(this.state.paypal.email).toLowerCase());
  }

  render() {
    const {
      handleDropdown,
      toggle,
      bankPayment,
      payPal,
      handleMasterCard,
      handlePayPal,
      items,
    } = this.props;
    const {
      paypal: { email },
      bankPayments: {
        bic,
        routing_number,
        bank_name,
        account_number,
        iban,
        name,
      },
      submitted,
      submitted2,
      currency,
      open,
    } = this.state;
    return (
      <>
        <div>
          <Button className="icon" onClick={this.handleModal} color="payment">
            <PlusIcon />
            add Payment
          </Button>
          <ModalFormPayment
            toggle={open}
            handleModal={this.handleModal}
            addPayment
            onSubmit={
              bankPayment ? this.handleSubmitBank : this.handleChangeSubmit
            }
            handleMasterCard={handleMasterCard}
            handlePayPal={handlePayPal}
            handleDropdown={handleDropdown}
            dropdown={toggle}
          >
            {bankPayment && (
              <>
                  <div className="form__form-group">
                    <span className="form__form-group-label">Currency</span>
                    <div className="form__form-group-select">
                      <div className="form__form-group-input-wrap">
                        <select
                          id="searchSelect"
                          name="searchSelect"
                          onChange={this.selectedValueHandler}
                          className="form__select"
                        >
                          {currency.map((item) => (
                            <option key={item.value}>{item.name}</option>
                          ))}
                        </select>
                      </div>
                    </div>
                  </div>
                <ModalFormBank
                  submitted={submitted}
                  selectedValue={this.state.selectedValue}
                  errors={this.state.ErrorMessage.bank && (
                      <div
                        id="validationServer03Feedback"
                        className="form__form-group-error"
                      >
                        {this.state.ErrorMessage.bank}
                      </div>
                    )}
                  errorsEuro={
                    this.state.ErrorMessage.bankEuro && (
                      <div
                        id="validationServer03Feedback"
                        className="form__form-group-error"
                      >
                        {this.state.ErrorMessage.bankEuro}
                      </div>
                    )
                  }
                  maxLength={maxLengthCheck}
                  onChange={(bank) => this.setState(bank)}
                  bankPayments={{ bank_name, name, iban, bic, routing_number, account_number }}
                />
                <button
                  className="btn btn-account account__btn account__btn--small"
                  style={{
                    marginTop: "10px",
                    color: "#fff",
                    width: "100%",
                    position: "relative",
                  }}
                  type="submit"
                >
                  Save
                </button>
              </>
            )}
            {payPal && (
              <>
                <ModalFormPayPal
                  submitted={submitted2}
                  paypal={{ email }}
                  onChange={(paypal) => this.setState(paypal)}
                />
                 {this.state.ErrorMessage.check_email && (
                  <div
                    id="validationServer03Feedback"
                    className="form__form-group-error"
                  >
                    {this.state.ErrorMessage.check_email}
                  </div>
                )}
                <button
                  className="btn btn-account account__btn account__btn--small"
                  style={{
                    marginTop: "10px",
                    color: "#fff",
                    width: "100%",
                    position: "relative",
                  }}
                  type="submit"
                >
                  Save
                </button>
              </>
            )}
          </ModalFormPayment>
        </div>
      </>
    );
  }
}

AddPayment.propTypes = {
  handleDropdown: PropTypes.func.isRequired,
  toggle: PropTypes.bool.isRequired,
  handleMasterCard: PropTypes.func.isRequired,
  handlePayPal: PropTypes.func.isRequired,
  bankPayment: PropTypes.bool.isRequired,
  payPal: PropTypes.bool.isRequired,
};

function mapStateToProps(state) {
  const { error, loading, items } = state.paymentMethod;
  return { error, loading, items };
}

const actionCreators = {
  createPayment: paymentActions.createPaymentMethod,
};

const connectedAddPayment = connect(
  mapStateToProps,
  actionCreators,
)(AddPayment);
export { connectedAddPayment as AddPayment };
