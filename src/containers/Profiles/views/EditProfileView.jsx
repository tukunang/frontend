// @flow

import React from "react";
import { connect } from "react-redux";
import { Container, Row } from "reactstrap";
import CircularProgress from "@mui/material/CircularProgress";
import { MenuSettings } from "../components/Menu/MenuSettings";
import { ProfileDetail } from "../components/ProfileDetail";

// eslint-disable-next-line react/prefer-stateless-function
class EditProfileView extends React.Component {
  render() {
    const { user } = this.props;
    return (
      <Container className="dashboard">
          <Row>
            <ProfileDetail
              first_name={user.first_name}
              last_name={user.last_name}
            >
              <MenuSettings />
            </ProfileDetail>
          </Row>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  const { user, loading } = state.userProfile;
  return { user, loading };
}

const EditProfiles = connect(mapStateToProps)(EditProfileView);

export default EditProfiles;
