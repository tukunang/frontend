// @flow

import React from "react";
import { faExclamation } from "@fortawesome/free-solid-svg-icons";
import { Button, Modal } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

type Props = {
    handleModalHide: () => void,
    handleDeletePayment: () => void,
    toggle: boolean,
};

function ModalDelete({
    handleModalHide,
    handleDeletePayment,
    toggle,
}: Props) {
  return (
    <>
      <Modal
        isOpen={toggle}
        className="modal-dialog--header modal-dialog--payment"
        centered
        toggle={handleModalHide}
      >
        <div className="modal__header">
          <h5 className="text-modal modal__title">
            <div className="modal__border-payment-warning">
              <FontAwesomeIcon
                className="card-warning-modal"
                icon={faExclamation}
              />
            </div>
          </h5>
          <h4 className="text-modal modal__title" style={{ marginTop: "20px" }}>
            are you sure delete this payment
          </h4>
        </div>
        <div className="modal__body">
          <Button
            type="submit"
            onClick={handleModalHide}
            style={{ marginRight: "8px" }}
            className="btn btn-cancel payment__btn payment__btn--small"
          >
            Cancel
          </Button>
          <Button
            type="submit"
            onClick={handleDeletePayment}
            style={{ marginLeft: "8px" }}
            className="btn btn-delete payment__btn payment__btn--small"
          >
            Delete
          </Button>
        </div>
      </Modal>
    </>
  );
}

export { ModalDelete };
