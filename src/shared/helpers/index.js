// @flow

const ligthTheme = {
  backgroundColor: "white",
  color: "#646777",
};

const darkTheme = {
  backgroundColor: "#2e2c34",
  color: "#dddddd",
};

export const themes = {
  ligthTheme,
  darkTheme,
};

export const emailPatter = /^[\w-]+(\.[\w-]+)*@([a-z0-9-]+(\.[a-z0-9-]+)*?\.[a-z]{2,6}|(\d{1,3}\.){3}\d{1,3})(:\d{4})?$/;

const getTooltipStyles = (themeName, type) => {
  switch (themeName) {
    case "theme-dark": {
      const { backgroundColor, color } = darkTheme;
      return {
        contentStyle: { backgroundColor },
        itemStyle: type === "defaultItems" ? null : { color },
      };
    }
    case "theme-light": {
      return ligthTheme;
    }
    default:
      return ligthTheme;
  }
};

export const maxLengthCheck = (object) => {
  if (object.target.value.length > object.target.maxLength) {
   object.target.value = object.target.value.slice(0, object.target.maxLength);
  }
};

export const Slice = (object, keys) => {
  return Object.keys(object)
      .filter(function (key) {
          return keys.indexOf(key) >= 0;
      })
      .reduce(function (acc, key) {
          acc[key] = object[key];
          return acc;
      }, {});
};

export default getTooltipStyles;

export const blockInvalidChar = e => ['e', 'E', '+', '-'].includes(e.key) && e.preventDefault();
export const blockInvalidSpecial = e => ['!', '@', '#', '$', '%', '^', '&', '*', '/', '<', '>', ';'].includes(e.key) && e.preventDefault();