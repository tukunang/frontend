// @flow

type CookieAttributes = {
    expires?: number | Date | undefined,
    path?: string | undefined,
    domain?: string | undefined,
    secure? : boolean | undefined,
    sameSite?: 'strict' | 'Strict' | 'lax' | 'Lax' | 'none' | 'None' | undefined,
};

export type CookieWriteConverter = (value: string, name: string) => string;
export type CookieReadConverter = (value: string, name: string) => string;

export type Converter = {
    read?: CookieReadConverter | undefined,
    write?: CookieWriteConverter | undefined,
};

export type CookiesStatic = {
    readonly converter: Required<Converter>,
    set(name: string, value: string, options: CookieAttributes): string | undefined,
    get(name: string): string | undefined,
    remove(name: string, options?: CookieAttributes): void;
};